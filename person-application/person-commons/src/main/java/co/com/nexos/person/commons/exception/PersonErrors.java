package co.com.nexos.person.commons.exception;

public enum PersonErrors {

	NO_DATA("No hay informacion para guardar"),
	EMAIL_DUPLICATE("Correo duplicado"),
	NO_EMAIL("El correo es necesario"),
	NO_FOUND("Persona no encontrada"),
	EMAIL("Correo invalido");

	private String message;

	PersonErrors(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
