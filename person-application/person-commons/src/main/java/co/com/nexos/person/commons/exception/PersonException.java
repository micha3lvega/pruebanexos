package co.com.nexos.person.commons.exception;

public class PersonException extends Exception {

	private static final long serialVersionUID = -3133264684315750214L;

	private final PersonErrors error;

	public PersonException(PersonErrors messaje) {
		super(messaje.getMessage());
		this.error = messaje;
	}

	public PersonException(PersonErrors messaje, Throwable err) {
		super(messaje.getMessage(), err);
		this.error = messaje;
	}

	public PersonException(String message) {
		super(message);
		this.error = null;
	}

	public PersonException(String message, Throwable err) {
		super(message, err);
		this.error = null;
	}

	public PersonErrors getError() {
		return error;
	}

}
