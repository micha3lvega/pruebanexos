package co.com.nexos.person.commons;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PersonDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String id;

	private String name;
	private String lastName;

	private Long age;
	private GenderDTO gender;

	private String email;
	private String phone;

	private Date birthdate;

}
