package co.com.nexos.person.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import co.com.nexos.person.commons.GenderDTO;
import co.com.nexos.person.services.model.Person;

@SpringBootTest
class PersonTest {

	private static Validator validator;

	@BeforeEach
	public void setupValidatorInstance() {
		validator = Validation.buildDefaultValidatorFactory().getValidator();
	}

	/**
	 * Prueba de las validaciones del documento de personas
	 */
	@Test
	void personDataTest() {

		Person person = Person.builder().
				name("Jhon").
				lastName("Doe").
				gender(GenderDTO.MALE).
				age(10L).
				email("me@example.com").
				phone("3000000000").
				birthdate(new Date()).
				build();

		Set<ConstraintViolation<Person>> violations = validator.validate(person);

		assertThat(violations.size()).isZero();

		// Name not null
		person.setName(null);
		violations = validator.validate(person);
		assertThat(violations.size()).isEqualTo(1);
		
		// LastName not null
		person.setLastName(null);
		violations = validator.validate(person);
		assertThat(violations.size()).isEqualTo(2);
		
		// Age not null
		person.setAge(null);
		violations = validator.validate(person);
		assertThat(violations.size()).isEqualTo(3);

		// Age less than 150
		person.setAge(151L);
		violations = validator.validate(person);
		assertThat(violations.size()).isEqualTo(3);

		// email not null
		person.setEmail(null);
		violations = validator.validate(person);
		assertThat(violations.size()).isEqualTo(4);
		
		// email not null
		person.setEmail("me@");
		violations = validator.validate(person);
		assertThat(violations.size()).isEqualTo(4);
		
		// phone not null
		person.setPhone(null);
		violations = validator.validate(person);
		assertThat(violations.size()).isEqualTo(5);
		
		// Birthdate not null
		person.setBirthdate(null);
		violations = validator.validate(person);
		assertThat(violations.size()).isEqualTo(6);
		
	}

}
