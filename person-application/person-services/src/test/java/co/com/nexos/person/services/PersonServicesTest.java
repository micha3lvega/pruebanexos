package co.com.nexos.person.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import co.com.nexos.person.commons.GenderDTO;
import co.com.nexos.person.commons.PersonDTO;
import co.com.nexos.person.commons.exception.PersonException;
import co.com.nexos.person.services.services.IPersonServices;

@SpringBootTest
class PersonServicesTest {

	@Autowired
	private IPersonServices personServices;

	private PersonDTO person = PersonDTO.builder().
			name("Jhon").
			lastName("Doe").
			gender(GenderDTO.MALE).
			age(10L).
			phone("3000000000").
			birthdate(new Date()).
			build();

	/**
	 * Probar la creacion de una persona
	 * 
	 * @throws PersonException
	 */
	@Test
	void testAddPerson() throws PersonException {

		person.setEmail(System.currentTimeMillis() + "@email.com");
		PersonDTO savePerson = personServices.save(person);

		assertThat(savePerson).isNotNull();
		assertThat(savePerson.getId()).isNotNull();

	}

	@Test
	void testUpdatePerson() throws PersonException {

		// Crear una persona
		person.setEmail(System.currentTimeMillis() + "@email.com");
		PersonDTO savePerson = personServices.save(person);

		assertThat(savePerson).isNotNull();
		assertThat(savePerson.getId()).isNotNull();

		// Actualizar persona
		String email = System.currentTimeMillis() + "@email.com";

		person.setEmail(email);
		savePerson = personServices.save(person);

		assertThat(savePerson).isNotNull();
		assertThat(savePerson.getId()).isNotNull();
		assertThat(savePerson.getEmail()).isEqualTo(email);

		// Buscar nuevamente la persona
		PersonDTO searchPerson = personServices.findById(savePerson.getId());

		assertThat(searchPerson).isNotNull();
		assertThat(searchPerson.getId()).isNotNull();
		assertThat(searchPerson.getEmail()).isEqualTo(email);

	}
	
	@Test()
	void notFoundPerson() {
		
		Assertions.assertThrows(PersonException.class, () -> {
			 personServices.findById("notfound");
		});
		
	}
	
	@Test
	void testDelete() throws PersonException {
		
		// Crear una persona
		person.setEmail(System.currentTimeMillis() + "@email.com");
		PersonDTO savePerson = personServices.save(person);

		assertThat(savePerson).isNotNull();
		assertThat(savePerson.getId()).isNotNull();
		
		//Borrarla
		personServices.delete(savePerson.getId());
		
		//Buscarla de nuevo
		Assertions.assertThrows(PersonException.class, () -> {
			 personServices.findById(savePerson.getId());
		});
		
	}
	
	@Test
	void testNoSaveRepeatEmail() throws PersonException {
		
		// Actualizar persona
		String email = System.currentTimeMillis() + "@email.com";

		person.setEmail(email);
		PersonDTO savePerson = personServices.save(person);

		assertThat(savePerson).isNotNull();
		assertThat(savePerson.getId()).isNotNull();
		assertThat(savePerson.getEmail()).isEqualTo(email);

		// Guardar nuevamente la persona
		savePerson.setId(null);
		Assertions.assertThrows(PersonException.class, () -> {
			 personServices.save(savePerson);
		});
		
	}
}
