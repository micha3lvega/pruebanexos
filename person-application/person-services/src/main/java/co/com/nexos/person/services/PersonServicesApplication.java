package co.com.nexos.person.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonServicesApplication.class, args);
	}

}
