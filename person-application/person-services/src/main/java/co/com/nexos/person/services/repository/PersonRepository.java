package co.com.nexos.person.services.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import co.com.nexos.person.services.model.Person;

public interface PersonRepository extends MongoRepository<Person, String> {

	Person findByEmail(String email);

}
