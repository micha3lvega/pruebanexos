package co.com.nexos.person.services.rest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.nexos.person.commons.PersonDTO;
import co.com.nexos.person.commons.exception.PersonErrors;
import co.com.nexos.person.commons.exception.PersonException;
import co.com.nexos.person.services.services.IPersonServices;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/person")
public class PersonRestController {

	@Autowired
	private IPersonServices iPersonServices;

	@GetMapping
	public ResponseEntity<List<PersonDTO>> findAll() {
		return new ResponseEntity<>(iPersonServices.findAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") String id) {

		try {
			return new ResponseEntity<>(iPersonServices.findById(id), HttpStatus.OK);
		} catch (PersonException e) {
			if (e.getError().equals(PersonErrors.NO_FOUND)) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/email/{email}")
	public ResponseEntity<?> findByEmail(@PathVariable("email") String email) {

		try {
			return new ResponseEntity<>(iPersonServices.findByEmail(email), HttpStatus.OK);
		} catch (PersonException e) {
			if (e.getError().equals(PersonErrors.NO_FOUND)) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping
	public ResponseEntity<?> save(@Valid @RequestBody PersonDTO person) throws PersonException {
		try {
			return new ResponseEntity<>(iPersonServices.save(person), HttpStatus.OK);
		} catch (PersonException e) {
			if (e.getError().equals(PersonErrors.EMAIL_DUPLICATE)) {
				return new ResponseEntity<>(PersonErrors.EMAIL_DUPLICATE.getMessage(), HttpStatus.CONFLICT);
			}
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);

		}
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") String id) throws PersonException {
		iPersonServices.delete(id);
	}

}
