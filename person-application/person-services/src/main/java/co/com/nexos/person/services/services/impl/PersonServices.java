package co.com.nexos.person.services.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.nexos.person.commons.PersonDTO;
import co.com.nexos.person.commons.exception.PersonErrors;
import co.com.nexos.person.commons.exception.PersonException;
import co.com.nexos.person.services.model.Person;
import co.com.nexos.person.services.repository.PersonRepository;
import co.com.nexos.person.services.services.IPersonServices;

@Service
public class PersonServices implements IPersonServices {

	@Autowired
	private PersonRepository repository;

	@Autowired
	private ModelMapper mapper;

	@Override
	public PersonDTO save(PersonDTO dto) throws PersonException {

		if (dto == null) {
			throw new PersonException(PersonErrors.NO_DATA);
		}

		if (dto.getEmail() == null || dto.getEmail().isBlank()) {
			throw new PersonException(PersonErrors.NO_EMAIL);
		}

		// Buscar que no se repita el email
		Person emailPerson = repository.findByEmail(dto.getEmail());

		if (emailPerson != null && !emailPerson.getId().equals(dto.getId())) {
			throw new PersonException(PersonErrors.EMAIL_DUPLICATE);
		}

		Person obj = mapper.map(dto, Person.class);
		return mapper.map(repository.save(obj), PersonDTO.class);

	}

	@Override
	public PersonDTO findById(String id) throws PersonException {
		Person person = repository.findById(id).orElseThrow(() -> new PersonException(PersonErrors.NO_FOUND));
		return mapper.map(person, PersonDTO.class);
	}

	@Override
	public PersonDTO findByEmail(String email) throws PersonException {
		Person person = repository.findByEmail(email);

		if (person == null) {
			throw new PersonException(PersonErrors.NO_FOUND);
		}
		
		return mapper.map(person, PersonDTO.class);
	}

	@Override
	public List<PersonDTO> findAll() {
		return repository.findAll().stream().map(person -> {
			return mapper.map(person, PersonDTO.class);
		}).collect(Collectors.toList());
	}

	@Override
	public void delete(String id) throws PersonException {

		// Busca la persona
		Person person = repository.findById(id).orElseThrow(() -> new PersonException(PersonErrors.NO_FOUND));

		// Borrarla
		repository.delete(person);
	}

}
