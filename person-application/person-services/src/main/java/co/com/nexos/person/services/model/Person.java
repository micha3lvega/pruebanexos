package co.com.nexos.person.services.model;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import co.com.nexos.person.commons.GenderDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Document
@NoArgsConstructor
@AllArgsConstructor
public class Person implements Serializable {

	private static final long serialVersionUID = 6887592033116888829L;

	@Id
	private String id;

	@Size(min = 2, max = 50, message = "El tamaño del nombre es invalido")
	@NotBlank(message = "El nombre no puede estar vacio")
	private String name;

	@Size(min = 2, max = 50, message = "El tamaño del nombre es invalido")
	@NotBlank(message = "El apellido no puede estar vacio")
	private String lastName;

	@NotNull
	@Max(value = 150, message = "Eres demasiado viejo")
	private Long age;

	private GenderDTO gender;

	@NotNull
	@Email(message = "El correo no tiene un formato valido") // easter egg
	private String email;

	@Size(min = 10, max = 10, message = "El telefono debe tener 10 digitos")
	@NotBlank(message = "El apellido no puede estar vacio")
	private String phone;

	@NotNull
	private Date birthdate;

	// Auditoria basica
	@CreatedDate
	private Date createDate;

	@LastModifiedDate
	private Date lastModifiedDate;
}
