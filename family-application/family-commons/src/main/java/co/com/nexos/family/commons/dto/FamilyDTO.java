package co.com.nexos.family.commons.dto;

import java.io.Serializable;
import java.util.List;

import co.com.nexos.person.commons.PersonDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FamilyDTO implements Serializable {

	private static final long serialVersionUID = -4627754329361328113L;

	private String id;

	private PersonDTO person;

	private List<FamilyMember> members;

}
