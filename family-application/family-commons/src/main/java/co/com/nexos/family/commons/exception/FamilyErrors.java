package co.com.nexos.family.commons.exception;

public enum FamilyErrors {

	NO_FOUND("Familia no encontrada"),
	NO_DATA("No hay informacion para guardar"),
	PERSON_NOT_FOUND("Persona no encontrada"),
	PERSON_REQUERIED("La persona principal es requerida"),
	PERSON_EMAIL_REQUERIED("El correo de la persona principal es requerida")
	;
	
	private String message;

	FamilyErrors(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
