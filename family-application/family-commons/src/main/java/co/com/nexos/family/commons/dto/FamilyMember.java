package co.com.nexos.family.commons.dto;

import java.io.Serializable;

import co.com.nexos.person.commons.PersonDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FamilyMember implements Serializable {

	private static final long serialVersionUID = 1L;

	private PersonDTO person;

	private RolFamily rol;

}
