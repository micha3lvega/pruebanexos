package co.com.nexos.family.commons.exception;

public class FamilyException extends Exception {

	private static final long serialVersionUID = 1973050207601531862L;
	private final FamilyErrors error;

	public FamilyException(FamilyErrors messaje) {
		super(messaje.getMessage());
		this.error = messaje;
	}

	public FamilyException(FamilyErrors messaje, Throwable err) {
		super(messaje.getMessage(), err);
		this.error = messaje;
	}

	public FamilyException(String message) {
		super(message);
		this.error = null;
	}

	public FamilyException(String message, Throwable err) {
		super(message, err);
		this.error = null;
	}

	public FamilyErrors getError() {
		return error;
	}

}
