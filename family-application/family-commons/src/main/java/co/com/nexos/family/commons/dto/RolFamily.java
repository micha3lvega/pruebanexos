package co.com.nexos.family.commons.dto;

public enum RolFamily {
	
	MOTHER, FATHER, BROTHER, SON, GRANDMOTHER, GRANDFATHER;

}
