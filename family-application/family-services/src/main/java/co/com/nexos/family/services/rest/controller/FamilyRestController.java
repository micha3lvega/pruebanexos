package co.com.nexos.family.services.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.nexos.family.commons.dto.FamilyDTO;
import co.com.nexos.family.commons.exception.FamilyException;
import co.com.nexos.family.services.services.IFamilyServices;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/family")
public class FamilyRestController {

	@Autowired
	private IFamilyServices iFamilyServices;

	/**
	 * Busca toda las entidades en la base de datos
	 * 
	 * @return Todas la entidades de tipo {@link Family}
	 */
	@GetMapping
	public List<FamilyDTO> findAll() {
		return iFamilyServices.findAll();
	}

	/**
	 * Busca una familia por su id
	 * 
	 * @param id
	 * @return la familia encontrada
	 */
	@GetMapping("/{id}")
	public FamilyDTO findById(@PathVariable("id") String id) throws FamilyException {
		return iFamilyServices.findById(id);
	}

	/**
	 * Metodo que busca a la familia de una persona por su ID
	 * 
	 * @param personId
	 * @return El objecto {@link Family} de una persona
	 */
	@GetMapping("/person/{id}")
	public FamilyDTO findByPerson(@PathVariable("id") String personId) throws FamilyException {
		return iFamilyServices.findByPerson(personId);
	}

	/**
	 * Metodo que guarda una familia en la base de datos
	 * 
	 * @param family
	 * @return la entidad {@link Family} guardada en la base de datos
	 */
	@PostMapping
	FamilyDTO save(@RequestBody FamilyDTO family) throws FamilyException {
		return iFamilyServices.save(family);
	}

	/**
	 * Metodo que eliminat una familia por su id
	 * 
	 * @param id
	 */
	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") String id) throws FamilyException {
		iFamilyServices.delete(id);
	}
}
