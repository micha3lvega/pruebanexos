package co.com.nexos.family.services.services.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import co.com.nexos.family.services.services.IPersonServices;
import co.com.nexos.person.commons.PersonDTO;
import co.com.nexos.person.commons.exception.PersonErrors;
import co.com.nexos.person.commons.exception.PersonException;

@Service
public class PersonServices implements IPersonServices {

	@Autowired
	private RestTemplate client;

	@Value("${person.application.url}")
	private String url;

	@Override
	public PersonDTO save(PersonDTO dto) throws PersonException {

		try {
			return client.postForObject(url, dto, PersonDTO.class);
		} catch (HttpClientErrorException e) {
			throw new PersonException(e.getMessage(), e);
		}

	}

	@Override
	public PersonDTO findById(String id) throws PersonException {

		try {
			return client.getForObject(url + id, PersonDTO.class);
		} catch (HttpClientErrorException e) {
			if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
				// person no encontrada
				throw new PersonException(PersonErrors.NO_FOUND);
			}

			throw new PersonException(e.getMessage(), e);
		}
	}

	@Override
	public List<PersonDTO> findAll() throws PersonException{

		try {
			return Arrays.asList(client.getForObject(url, PersonDTO[].class));
		} catch (HttpClientErrorException e) {
			throw new PersonException(e.getMessage(), e);
		}
	}

	@Override
	public void delete(String id) throws PersonException {
		try {
			client.delete(url + id);
		} catch (HttpClientErrorException e) {

			if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
				// person no encontrada
				throw new PersonException(PersonErrors.NO_FOUND);
			}

			throw new PersonException(e.getMessage(), e);
		}
	}

	@Override
	public PersonDTO findByEmail(String email) throws PersonException {
		try {
			return client.getForObject(url + "email/" + email, PersonDTO.class);
		} catch (HttpClientErrorException e) {

			if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
				// person no encontrada
				return null;
			}

			throw new PersonException(e.getMessage(), e);
		}
	}

}
