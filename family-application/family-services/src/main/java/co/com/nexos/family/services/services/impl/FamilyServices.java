package co.com.nexos.family.services.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.nexos.family.commons.dto.FamilyDTO;
import co.com.nexos.family.commons.dto.FamilyMember;
import co.com.nexos.person.commons.PersonDTO;
import co.com.nexos.person.commons.exception.PersonException;
import co.com.nexos.family.commons.exception.FamilyErrors;
import co.com.nexos.family.commons.exception.FamilyException;
import co.com.nexos.family.services.model.Family;
import co.com.nexos.family.services.repository.FamilyRepository;
import co.com.nexos.family.services.services.IFamilyServices;

@Service
public class FamilyServices implements IFamilyServices {

	@Autowired
	private FamilyRepository repository;

	@Autowired
	private PersonServices personServices;

	@Autowired
	private ModelMapper mapper;

	@Override
	public List<FamilyDTO> findAll() {
		return repository.findAll().stream().map(family -> {
			return mapper.map(family, FamilyDTO.class);
		}).collect(Collectors.toList());
	}

	@Override
	public FamilyDTO findByPerson(String personId) throws FamilyException {
		
		//Buscarlo como principal		
		Family family =  repository.findById(personId).orElse(null);
		if (family != null) {
			return family;
		}
		
		//Buscarlo como miembro 
		return repository.findByMembersPersonIdIn(personId);
		
	}

	@Override
	public FamilyDTO save(FamilyDTO family) throws FamilyException {

		if (family == null) {
			throw new FamilyException(FamilyErrors.NO_DATA);
		}

		if (family.getPerson() == null) {
			throw new FamilyException(FamilyErrors.PERSON_REQUERIED);
		}

		try {
			
			// Buscar si la persona ya existe por el email
			PersonDTO searchPerson = personServices.findByEmail(family.getPerson().getEmail());
			
			if (searchPerson != null && searchPerson.getId() != null) {
				family.getPerson().setId(searchPerson.getId());// Con esto se actualiza la informacion de la persona
			}

			// Guardar o actualizar la persona principal
			family.setPerson(personServices.save(family.getPerson()));

			List<FamilyMember> updateMembers = new ArrayList<>();
			// Guardar o actualizar los miebros de la familia
			for (FamilyMember familyMember : family.getMembers()) {
				
				PersonDTO searchPersonMember = personServices.findByEmail(familyMember.getPerson().getEmail());
				
				if (searchPersonMember != null && searchPersonMember.getId() != null) {
					// Con esto se actualiza la informacion de la persona
					familyMember.getPerson().setId(searchPersonMember.getId());
				}
				
				PersonDTO person = personServices.save(familyMember.getPerson());
				System.out.println("Persona guardada: " + person);
				FamilyMember updateFamilyMember = new FamilyMember(person, familyMember.getRol());
				updateMembers.add(updateFamilyMember);
			}

			family.setMembers(updateMembers);

		} catch (PersonException e) {
			throw new FamilyException(e.getMessage(), e);
		}

		return mapper.map(repository.save(mapper.map(family, Family.class)), FamilyDTO.class);
	}

	@Override
	public void delete(String id) throws FamilyException {

		// Buscar si existe la familia
		Family family = repository.findById(id).orElseThrow(() -> new FamilyException(FamilyErrors.NO_FOUND));

		if (family == null) {
			throw new FamilyException(FamilyErrors.NO_DATA);
		}

		if (family.getPerson() == null) {
			throw new FamilyException(FamilyErrors.PERSON_REQUERIED);
		}

		try {

			// Guardar o actualizar la persona principal
			personServices.delete(family.getPerson().getId());

			// Guardar o actualizar los miebros de la familia
			for (FamilyMember familyMember : family.getMembers()) {
				personServices.delete(familyMember.getPerson().getId());
			}

		} catch (PersonException e) {
			throw new FamilyException(e.getMessage(), e);
		}

		// Borrar la familia
		repository.delete(family);

	}

	@Override
	public FamilyDTO findById(String id) throws FamilyException {
		return mapper.map(repository.findById(id).orElseThrow(() -> new FamilyException(FamilyErrors.NO_FOUND)),
				FamilyDTO.class);
	}

}
