package co.com.nexos.family.services.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import co.com.nexos.family.services.model.Family;

public interface FamilyRepository extends MongoRepository<Family, String> {

	Family findByPersonId(String id);

	Family findByMembersPersonIdIn(String... id);

}
