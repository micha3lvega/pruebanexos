package co.com.nexos.family.services.services;

import java.util.List;

import co.com.nexos.person.commons.PersonDTO;
import co.com.nexos.person.commons.exception.PersonException;

public interface IPersonServices {

	/**
	 * Metodo que guarda una persona en la base de datos
	 * 
	 * @param dto
	 * @return la entidad guardada
	 */
	PersonDTO save(PersonDTO dto) throws PersonException;

	/**
	 * Busca una entidad por su ID
	 * 
	 * @param id
	 * @return la entidad con el ID dado o null si no se encuentra.
	 */
	PersonDTO findById(String id) throws PersonException;
	

	/**
	 * Busca una entidad por su email
	 * 
	 * @param email
	 * @return la entidad con el email dado o null si no se encuentra.
	 */
	public PersonDTO findByEmail(String email) throws PersonException;

	/**
	 * Busca toda las entidades en la base de datos
	 * 
	 * @return todas las entidades
	 * @throws PersonException 
	 */
	List<PersonDTO> findAll() throws PersonException;

	/**
	 * Elimina la entidad con el ID recibido.
	 * 
	 * @param id
	 */
	void delete(String id) throws PersonException;

}
