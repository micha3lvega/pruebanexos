package co.com.nexos.family.services.services;

import java.util.List;

import co.com.nexos.family.commons.dto.FamilyDTO;
import co.com.nexos.family.commons.dto.RolFamily;
import co.com.nexos.family.commons.exception.FamilyException;
import co.com.nexos.person.commons.PersonDTO;

public interface IFamilyServices {

	/**
	 * Busca toda las entidades en la base de datos
	 * 
	 * @return Todas la entidades de tipo {@link Family}
	 */
	List<FamilyDTO> findAll();

	/**
	 * Busca una familia por su id
	 * 
	 * @param id
	 * @return la familia encontrada
	 */
	FamilyDTO findById(String id) throws FamilyException;

	/**
	 * Metodo que busca a la familia de una persona por su ID
	 * 
	 * @param personId
	 * @return El objecto {@link Family} de una persona
	 */
	FamilyDTO findByPerson(String personId) throws FamilyException;

	/**
	 * Metodo que guarda una familia en la base de datos
	 * 
	 * @param family
	 * @return la entidad {@link Family} guardada en la base de datos
	 */
	FamilyDTO save(FamilyDTO family) throws FamilyException;

	/**
	 * Metodo que eliminat una familia por su id
	 * 
	 * @param id
	 */
	void delete(String id) throws FamilyException;
}
