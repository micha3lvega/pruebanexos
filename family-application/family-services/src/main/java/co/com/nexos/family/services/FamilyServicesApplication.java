package co.com.nexos.family.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FamilyServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(FamilyServicesApplication.class, args);
	}

}
