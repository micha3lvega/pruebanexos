package co.com.nexos.family.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import co.com.nexos.family.commons.dto.FamilyDTO;
import co.com.nexos.family.commons.dto.FamilyMember;
import co.com.nexos.family.commons.dto.RolFamily;
import co.com.nexos.family.commons.exception.FamilyException;
import co.com.nexos.family.services.services.IFamilyServices;
import co.com.nexos.person.commons.GenderDTO;
import co.com.nexos.person.commons.PersonDTO;

@SpringBootTest
class FamilyServicesTests {

	@Autowired
	private IFamilyServices familyServices;

	private PersonDTO me = PersonDTO.builder().name("me").lastName("me")
			.email(System.currentTimeMillis() + "me@example.com").gender(GenderDTO.MALE).age(10L).phone("3000000000")
			.birthdate(new Date()).build();

	private PersonDTO father = PersonDTO.builder().name("Jhon").lastName("Doe")
			.email(System.currentTimeMillis() + "Jhon@example.com").gender(GenderDTO.MALE).age(10L).phone("3000000000")
			.birthdate(new Date()).build();

	private PersonDTO mother = PersonDTO.builder().name("Johana").lastName("Doe")
			.email(System.currentTimeMillis() + "Johana@example.com").gender(GenderDTO.FEMALE).age(10L)
			.phone("3000000000").birthdate(new Date()).build();

	private String id;

	FamilyDTO generateFamily() {

		FamilyMember father = FamilyMember.builder().person(this.father).rol(RolFamily.FATHER).build();
		FamilyMember mother = FamilyMember.builder().person(this.mother).rol(RolFamily.MOTHER).build();

		return FamilyDTO.builder().person(me).members(Arrays.asList(father, mother)).build();

	}

	@Test
	void testAddFamily() throws FamilyException {

		FamilyDTO family = generateFamily();

		// Guardar la familia
		family = familyServices.save(family);

		assertThat(family).isNotNull();
		assertThat(family.getId()).isNotNull();

	}

	@Test
	void validateEstructure() throws FamilyException {

		FamilyDTO family = generateFamily();

		// Guardar la familia
		family = familyServices.save(family);

		assertThat(family).isNotNull();
		assertThat(family.getId()).isNotNull();

		// Buscar la familia
		FamilyDTO searchFamily = familyServices.findById(family.getId());
		assertThat(searchFamily).isNotNull();
		assertThat(searchFamily.getId()).isNotNull();

	}

	@Test
	void validateDelete() throws FamilyException {

		FamilyDTO family = generateFamily();

		// Guardar la familia
		family = familyServices.save(family);

		assertThat(family).isNotNull();
		assertThat(family.getId()).isNotNull();

		// Buscar la familia
		FamilyDTO searchFamily = familyServices.findById(family.getId());
		assertThat(searchFamily).isNotNull();
		assertThat(searchFamily.getId()).isNotNull();

		// Eliminar
		this.id = searchFamily.getId();
		familyServices.delete(searchFamily.getId());

		Assertions.assertThrows(FamilyException.class, () -> {
			familyServices.findById(id);
		});

	}

}
